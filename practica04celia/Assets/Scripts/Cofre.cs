using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre : MonoBehaviour
{
    Animator animator;
    //Moneda
    public GameObject moneda;
    //Compara con las monedas que han salido ya
    public int limite;
    //Las monedas que va a haber en el cofre
    public int numeroMonedas;
    //Para que no sigan saliendo monedas despu�s de cogerlas
    public bool stop;
    //Sistema de particulas
    public ParticleSystem brillitos;
   

    // Start is called before the first frame update
    void Start()
    {
        //stop = false;
        animator = GetComponent<Animator>();
        animator.SetBool("Chest", false);
        brillitos.Stop();
    }
  
    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("1");
        if (collision.CompareTag("Player") && !stop)
        {
            Debug.Log("2");
            if (Input.GetKey(KeyCode.W))
            {
                Debug.Log("3");
                animator.SetBool("Chest", true);
              
                //Bucle: si el regulador es mayor que las monedas, se instancia+
                
                
                    Debug.Log("4");
                    for(int i=0; i<numeroMonedas;i++)
                    Instantiate(moneda, transform.position+new Vector3(0,i,0), Quaternion.identity);
                brillitos.Play();
                stop = true;
                    limite++;
                
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("5");
    }
}
