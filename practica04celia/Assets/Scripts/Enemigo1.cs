using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemigo1 : MonoBehaviour
{ public GameObject Mochanguito;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(Mochanguito);
        SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
    }
}
