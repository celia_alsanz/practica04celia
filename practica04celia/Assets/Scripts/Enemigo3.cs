using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemigo3 : MonoBehaviour
{
    public GameObject Mochanguito;
    public Animator Choque;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(Mochanguito);
        SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Choque.SetBool("Choque", true);

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Choque.SetBool("Choque", false);
    }
}
