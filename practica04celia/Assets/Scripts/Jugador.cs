using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Jugador : MonoBehaviour
{
     [Range(1, 10)]
    public float velocidad;
    [Range(1, 500)]
    public float potenciaSalto;
    Rigidbody2D rb2d;
    SpriteRenderer spRd;
    public Animator movimientoNaranja;
    
    //Detecta si esta en el suelo
    public LayerMask Mask; 

    
    void Start () {

        //Capturo los componentes Rigidbody2D y Sprite Renderer del Jugador
        rb2d = GetComponent<Rigidbody2D>();
        spRd = GetComponent<SpriteRenderer>();

    }
	
	void Update () {

        //Movimiento horizontal
        float movimientoH = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(movimientoH * velocidad, rb2d.velocity.y);

        //Sentido horizontal
        if (movimientoH > 0)
        {
            spRd.flipX = false;
            movimientoNaranja.SetBool("Walk", true);

        }
        else if (movimientoH < 0)
        {
            spRd.flipX = true;
            movimientoNaranja.SetBool("Walk", true);
        }

        //Animacion andar
        if (movimientoH == 0)
        {
            movimientoNaranja.SetBool("Walk", false);
        }




            jumpCheck();
            
            //Salto

        if (Input.GetKeyDown(KeyCode.Space) && Mathf.Abs(rb2d.velocity.y) < 0.001F && jumpCheck())
        {
            rb2d.AddForce(new Vector2(0, potenciaSalto), ForceMode2D.Impulse);
         
           
           
        }
        if (rb2d.velocity.y > 0)
        {
         

            movimientoNaranja.SetBool("Salto1", true);
        }
        else if (rb2d.velocity.y == 0)
        {
            movimientoNaranja.SetBool("Salto1", false);
        }
        if (rb2d.velocity.y < 0)
        {
        
            movimientoNaranja.SetBool("Salto2", true);
        }
        else if (rb2d.velocity.y == 0)
        {
            movimientoNaranja.SetBool("Salto2", false);
        }

        //Muerte por caida
        if(transform.position.y <= -14)
        {
            Destroy(gameObject);
            SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
        }


    }
  

    //Checkea suelo
    private bool jumpCheck()
       
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 10f;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.DrawRay(position,direction, Color.green, 2);
            //                                              Baja -1 Va hacia abajo
            RaycastHit2D hit = Physics2D.Raycast(position, direction,distance,Mask);
            if ((hit.collider != null))
            {
                Debug.Log(hit.collider.name);
                return true;
            }
            
        }
        return false;
    }
   
  

}

